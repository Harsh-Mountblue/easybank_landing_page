let temp = true;
const hamburger = document.querySelector(".hamburger-icon");
// const navMenu = document.querySelector(".nav-menu");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {

    if(temp) {
        document.getElementById('menu-options').style.zIndex = 3;
        document.getElementById('menu-options').style.display = "block";
        document.getElementById('phone').style.display = "none";
        document.getElementById('hamburger').style.height = "19px";
        document.getElementById('hamburger').style.width = "18px";
        document.getElementById('hamburger').style.background = "url('./images/icon-close.svg') no-repeat"


        temp = false;
    } else {
        document.getElementById('menu-options').style.zIndex = 0;
        document.getElementById('menu-options').style.display = "none";
        document.getElementById('phone').style.display = "block";
        document.getElementById('hamburger').style.background = "url('./images/icon-hamburger.svg') no-repeat"

        temp = true;
    }
}